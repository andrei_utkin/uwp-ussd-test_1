﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.NetworkOperators;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Background;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace uwp_1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }


        private async void GetBalance(object sender, RoutedEventArgs e)
        {
            try
            {
                // Get the network account ID.
                IReadOnlyList<string> networkAccIds = Windows.Networking.NetworkOperators.MobileBroadbandAccount.AvailableNetworkAccountIds;

                if (networkAccIds.Count == 0)
                {
                    //rootPage.NotifyUser("No network account ID found", NotifyType.ErrorMessage);
                    return;
                }
                // For the sake of simplicity, assume we want to use the first account.
                // Refer to the MobileBroadbandAccount API's for how to select a specific account ID.
                string networkAccountId = networkAccIds[0];

                //SendButton.IsEnabled = false;
                //rootPage.NotifyUser("Sending USSD request", NotifyType.StatusMessage);

                // Create a USSD session for the specified network acccount ID.
                UssdSession session = UssdSession.CreateFromNetworkAccountId(networkAccountId);

                // Send a message to the network and wait for the reply. This message is
                // specific to the network operator and must be selected accordingly.
                UssdReply reply = await session.SendMessageAndGetReplyAsync(new UssdMessage("*100#"));

                // Display the network reply. The reply always contains a ResultCode.
                UssdResultCode code = reply.ResultCode;
                if (code == UssdResultCode.ActionRequired || code == UssdResultCode.NoActionRequired)
                {
                    // If the actionRequired or noActionRequired ResultCode is returned, the reply contains
                    // a message from the network.
                    UssdMessage replyMessage = reply.Message;
                    string payloadAsText = replyMessage.PayloadAsText;
                    if (payloadAsText != "")
                    {
                        // The message may be sent using various encodings. If Windows supports
                        // the encoding, the message can be accessed as text and will not be empty.
                        // Therefore, the test for an empty string is sufficient.
                        //rootPage.NotifyUser("Response: " + payloadAsText, NotifyType.StatusMessage);
                    }
                    else
                    {
                        // If Windows does not support the encoding, the application may check
                        // the DataCodingScheme used for encoding and access the raw message
                        // through replyMessage.GetPayload
                        //rootPage.NotifyUser("Unsupported data coding scheme 0x" + replyMessage.DataCodingScheme.ToString("X"),NotifyType.StatusMessage);
                    }
                }
                else
                {
                    //rootPage.NotifyUser("Request failed: " + code.ToString(), NotifyType.StatusMessage);
                }
                if (code == UssdResultCode.ActionRequired)
                {
                    session.Close(); // Close the session from our end
                }
            }
            catch (Exception ex)
            {
                TB1.Text = "Unexpected exception occured: " + ex.ToString();
            }

            //IReadOnlyList<string> networkAccIds = MobileBroadbandAccount.AvailableNetworkAccountIds;
            //TB1.Text = networkAccIds.Count.ToString();
            //if (networkAccIds.Count == 0)
            //{
            //    //rootPage.NotifyUser("No network account ID found", NotifyType.ErrorMessage);
            //    return;
            //}
            //// For the sake of simplicity, assume we want to use the first account.
            //// Refer to the MobileBroadbandAccount API's for how to select a specific account ID.
            //string networkAccountId = networkAccIds[0];

            ////SendButton.IsEnabled = false;
            ////rootPage.NotifyUser("Sending USSD request", NotifyType.StatusMessage);

            //// Create a USSD session for the specified network acccount ID.
            //UssdSession session = UssdSession.CreateFromNetworkAccountId(networkAccIds[0]);

            //// Send a message to the network and wait for the reply. This message is
            //// specific to the network operator and must be selected accordingly.
            //var result = session.SendMessageAndGetReplyAsync(new UssdMessage("*100#")).GetResults();
            ////UssdReply reply = await session.SendMessageAndGetReplyAsync(new UssdMessage("100"));
        }

    }
}


